universities = [
    ["California Institute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachusetts Institute of Technology", 10566, 40732],
    ["Princeton", 7802, 37000],
    ["Rice", 5879, 35551],
    ["Stanford", 19535, 40569],
    ["Yale", 11701, 40500],
]

def enrollment_stats(list_of_universities):

    students_values = []
    tuition_fees = []

    for uni in list_of_universities:
        students_values.append(uni[1])
        tuition_fees.append(uni[2])

    return students_values, tuition_fees

def mean(new_list):
    return sum(new_list) / len(new_list)

def median(new_list):
    new_list.sort()
    if len(new_list) % 2 == 1:
        median_index = int(len(new_list) / 2)
        return new_list[median_index]
    else:
        left_center_index = (len(new_list) - 1) // 2
        right_center_index = (len(new_list) + 1) // 2
        return mean([new_list[left_center_index], new_list[right_center_index]])

totals = enrollment_stats(universities)

print("\n")
print("*****" * 6)
print(f"Total students:   {sum(totals[0]):,}")
print(f"Total tuition:  $ {sum(totals[1]):,}")
print(f"\nStudent mean:     {mean(totals[0]):,.2f}")
print(f"Student median:   {median(totals[0]):,}")
print(f"\nTuition mean:   $ {mean(totals[1]):,.2f}")
print(f"Tuition median: $ {median(totals[1]):,}")
print("*****" * 6)
print("\n")   

    
        
