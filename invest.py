def invest(amount, rate, years):
    """Calculates the profit from the given amount, rate and period """
    for year in range(1,years + 1):
        amount += amount * rate 
        print(f"year {year}: ${amount:,.2f}")
        

amount = int(input("An inital deposit: "))
rate = float(input("Annual rate - only decimal numbers: "))
years = int(input("Number of years: "))
invest(amount, rate, years)




