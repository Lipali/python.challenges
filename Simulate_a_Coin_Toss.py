import random

def coin_flip():
    """Filp a fair coin"""
    if random.randint(0,1) == 1:
        return "heads"
    else:
        return "tails"

trials_num = 10000
seq_num = 0
total_heads = 0
total_tails = 0

for trial in range(trials_num):
    if coin_flip() == "heads":
       total_heads = total_heads + 1
    else:
        total_tails = total_tails + 1
  
    if total_heads != 0 and total_tails != 0:
       seq_num = seq_num + 1
       total_heads = 0
       total_tails = 0
            
flips_avr = trials_num / seq_num
    
print(f"{flips_avr} flips on average needed for the sequence")
   
        
