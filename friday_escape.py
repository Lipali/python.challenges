def play_again():
    print("\nDo you want to play again? (y or n)")

    answer = input(">>>").lower()

    if answer == "y":
        start()
    else:
        exit()

def game_over(reason):
    print("\n" + reason)
    print("Game Over")
    play_again()

def kitchen_room():
    print("\nNareście dotarłeś do kuchni, to już ostatnia prosta, widzisz tylne dzwi wyjściowe.")
    print("\nFajnie by było wziąśc kilka zimnych piwek na drogę, ale ostatnią przeszkodą jest pies leżący koło lodówki.")
    print("\nBijesz się z własnymi myślami, próbujesz wygrać z pragnieniem, co robisz.... (1 or 2)")
    print("\n1.) Nie ma co ryzykować.. zatrzymam się w sklepie w drodzę na mecz.")
    print("\n2.) Nie ma takiej opcji, muszę trzasnąć pifko albo 2.")

    answer = input(">>>")

    if answer == "1":
          print("\nGratulacje udało ci się ucieć w ten piątkowy wieczór.")
          print("\nBiegniesz w stronę samochodu, słysząc kolegów skandujących twoje imię.")
          play_again()
    elif answer == "2":
        game_over('Przez przypadek nadepnąłeś psu na ogoń, pies zaczął okropnie skomleć do kuchni wbiega żona z teściową.')
    else:
        game_over("Lepiej zostań w domu i poćwicz pisanie cyferek.")
        
def mother_room():
    print('\nWchodzisz do pokoju i widzisz tesciową ogladającą "Moda na Sukces", ukrywasz się za lampą.')
    print("\nNa sam jej widok twoja krew się gotuje co robisz.... (1 or 2)")
    print('1.) Nie wytrzymujesz...i mówisz "Mamusi" co o niej myślisz.')
    print('2.) Uspokajasz samego siebie słowami: "Mecz jest ważniejszy, szkoda twoich nerwów") i skradasz się w stronę dzwi.')

    answer = input(">>>")

    if answer == "1":
        game_over('Tesciowa krzyczy i jednocześnie płacze, twoja żona wbiega do pokoju rozwścieczona i mówi: "A ty co zrobileś Mamusi..., gdzie ty wogóle się wybierasz".')
    elif answer == "2":
        print('\nBez problemu pominołeś "Kanapową bestie".')
        kitchen_room()
    else:
        game_over("Lepiej zostań w domu i poćwicz pisanie cyferek.")

def wife_room():
    print("\nWchodzisz do pokoju i widzisz żone próbującą uspać niemowle, szybko chowasz sie za sofe.")
    print("\nSerce bije jak szalone, ale wiesz że to jedyna droga, bo dzwi są zaraz koło łożeczka.")
    print("\nCo teraz zrobisz, masz dwa wyjśćia....(1 or 2)")
    print("1.) Spytać żone o pozwolenie na wyjście z kolegami.")
    print("2.) Bezszelestnie przeczołgać sie w strone dzwi.")
    
    answer = input(">>>")

    if answer == "1":
        game_over('Żona wpada w furię i zaczyna krzyczeć: "Przecież dzisiaj jest nasza rocznica... znowu zapomniałeś !!!" rozpętałeś piekło na ziemi')
    elif answer == "2":
        print("Udało się żona cię nie zauważyła")
        kitchen_room()
    else:
        game_over("Nie wiesz jak wpsiać numer ?")
def start():
    print("\nJest Piątek wieczor, własnie dostałes telefon od kolegi, że zdobył bilety na mecz waszego ulubionego klubu i zaraz po ciebie bedzie.")
    print("\nNie mozesz przepuścic takiej okazji, szybko przebierasz sie w barwy klubowe i wychodzisz z pokoju...")
    print("\nWidzisz dzwi po lewej i prawej stronie, które wybiereasz ?? (left or righ)")

    answer = input(">>>").lower()

    if answer == "left":
        wife_room()
    elif answer == "right":
        mother_room()
    else:
        game_over("Nie wiesz jak napisać coś poprawnie ?")
    
start()
