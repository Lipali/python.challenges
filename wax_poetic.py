import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

my_nouns = []
my_verbs = []
my_adjectives = []
my_prepositions = []
my_adverbs = []

for i in range(3):
    noun = random.choice(Nouns)
    my_nouns.append(noun)

    verb = random.choice(Verbs)
    my_verbs.append(verb)

    adjective = random.choice(Adjectives)
    my_adjectives.append(adjective)

for i in range(2):
    preposition = random.choice(Prepositions)
    my_prepositions.append(preposition)

    adverb = random.choice(Adverbs)
    my_adverbs.append(adverb)

vowels = ["a", "e", "i", "o", "u"]

if my_adjectives[0][0] in vowels:
    first_article = "An"
else:
    first_article = "A"

if my_adjectives[2][0] in vowels:
    second_article = "an"
else:
    second_article = "a"

print(f"{first_article} {my_adjectives[0]} {my_nouns[0]}")

print(f"{first_article} {my_adjectives[0]} {my_nouns[0]} {my_verbs[0]} {my_prepositions[0]} the {my_adjectives[1]} {my_nouns[1]}")
print(f"{my_adverbs[0]}, the {my_nouns[0]} {my_verbs[1]}")
print(f"the {my_nouns[1]} {my_verbs[2]} {my_prepositions[1]} {second_article} {my_adjectives[2]} {my_nouns[2]}")
