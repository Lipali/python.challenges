from pathlib import Path
import csv

scores = [
    {"name": "LLCoolDave", "score": 23},
    {"name": "LLCoolDave", "score": 27},
    {"name": "red", "score": 12},
    {"name": "LLCoolDave", "score": 26},
    {"name": "tom123", "score": 26},
    {"name": "Misha46", "score": 21},
    {"name": "Misha46", "score": 25},
    {"name": "Empiro", "score": 23},
    {"name": "Empiro", "score": 10},
    ]

file_path = Path.home() / "scores.csv"

with file_path.open(mode="w", newline='', encoding="utf-8") as file:
    writer = csv.DictWriter(file, fieldnames=scores[0].keys())
    writer.writeheader()
    writer.writerows(scores)
   
scores = []
with file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.DictReader(file)
    for row in reader:
        scores.append(row)

high_scores = {}
for row in scores:
    name = row["name"]
    score = int(row["score"])
    if name not in high_scores:
        high_scores[name] = score
    else:
        if high_scores[name] < score:
            high_scores[name] = score

new_file_path = Path.home() / "high_scores.csv"
with new_file_path.open(mode="w", newline="", encoding="utf-8") as new_file:
    writer = csv.DictWriter(new_file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for name in high_scores:
        row_dict = {"name": name, "high_score": high_scores[name]}
        writer.writerow(row_dict)
    

        



    
