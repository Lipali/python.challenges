input_1 = input("Enter a base: ")
input_2 = input("Enter an exponent: ")
number_raised = float(input_1) ** float(input_2)
print(f"{input_1} to the power of {input_2} = {number_raised}")
